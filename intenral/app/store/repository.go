package store

import (
	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/model"
)

// RatesRepository ...
type RatesRepository interface {
	Create(*model.Rates) error
	GetExtremums() (max_date string, end_date string, err error)
	GetRates(start_date string, end_date string) []map[string]string
	FetchRates(start_date string, end_date string, token string, remote_url string) []map[string]string
}

type TokenRepository interface { 
	FindByToken(token string) (*model.Token, error)
}