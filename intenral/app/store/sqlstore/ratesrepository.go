package sqlstore

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/model"
)

// RatesRepository ...
type RatesRepository struct {
	store *Store
}

func (r *RatesRepository) Create(u *model.Rates) error { 
	return nil
}


func (r *RatesRepository) GetExtremums() (max_date string, end_date string, err error){ 
	if err := r.store.db.QueryRow(
		"SELECT MAX(parse_date) AS max_date, MIN(parse_date) AS min_date FROM rates",
	).Scan(&max_date, &end_date); err != nil { 
		return max_date, end_date, err
	}
	return max_date, end_date, nil
}

func (r *RatesRepository) GetRates(start_date string, end_date string) []map[string]string {

	q := `SELECT arse_date, currency_from, currency_to, rate FROM rates WHERE parse_date >= $1 and parse_date <= $2`

	rows, err := r.store.db.Query(q, start_date, end_date)
	if err != nil {
		log.Println(err)
		return nil
	}
	defer rows.Close()

	var arr []map[string]string

	for rows.Next() {
		var (
			parse_date   string
			currency_from string
			currency_to string
			rate string
		)
		if err := rows.Scan(&parse_date, &currency_from, &currency_to, &rate); err != nil {
			log.Println(err)
			return nil
		}
		d := make(map[string]string)
		// date string in RFC1123 format to "2006-01-02"
		d["curr_date"] = parse_date[:10]
		d["curr_from"] = currency_from
		d["curr_to"] = currency_to
		d["rate"] = rate
		arr = append(arr, d)
	}
	return arr
}



type CustomTime struct {
	DateString string
}

type Rate struct { 
	Date CustomTime `json:"date"` 
	From string `json:"from"` 
	To string `json:"to"` 
	Rate float64 `json:"rate"`

}

type RatesAPIResponse struct {
	Status string `json:"status"`
	Data []Rate `json:"data"`
}


func (ct *CustomTime) UnmarshalJSON(b []byte) (err error) {
	s := string(b)
	s = s[1:len(s)-1]
	t, _ := time.Parse(time.RFC1123, s)
	ct.DateString = t.Format("2006-01-02")
	return
}

func InsertRates(arr []Rate, r *RatesRepository) ([]map[string]string, error){
	sqlStr := "INSERT INTO rates(parse_date, currency_from, currency_to, rate) VALUES "
	vals := []interface{}{}

	var rates []map[string]string

	for _, row := range arr {
		sqlStr += "(?, ?, ?, ?),"
		vals = append(vals, row.Date.DateString, row.From, row.To, row.Rate)
		d := make(map[string]string)
		d["curr_date"] = row.Date.DateString
		d["curr_from"] = row.From
		d["curr_to"] = row.To
		d["rate"] = fmt.Sprintf("%f", row.Rate)
		rates = append(rates, d)
	}

	//trim the last ,
	sqlStr = sqlStr[0:len(sqlStr)-1]
	//prepare the statement
	stmt, _ := r.store.db.Prepare(sqlStr)
	//format all vals at once
	fmt.Println(stmt, vals)
	_, err := stmt.Exec(vals...)
	return rates, err
}

func (r *RatesRepository) FetchRates(start_date string, end_date string, token string, remote_url string) []map[string]string {
	q := `DELETE FROM rates WHERE parse_date >= $1 and parse_date <= $2`
	_, err := r.store.db.Exec(q, start_date, end_date)
	if err != nil { 
		log.Fatal(err)
	}

	base_url := fmt.Sprintf("https://%s/api/currencies/exchange_rate", remote_url)
	url := fmt.Sprintf("%s?token=%s&start_date=%s&end_date=%s", base_url, token, start_date, end_date)


	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify : true},
	}
	client := &http.Client{Transport: tr}
	res, err := client.Get(url)
	if err != nil {
		log.Println(err)
		return nil
    }

	body, _ := ioutil.ReadAll(res.Body)
	
	var rr = new(RatesAPIResponse)
    err = json.Unmarshal(body, &rr)
	if err != nil {
		log.Println(err)
		return nil
	}
	rates, err := InsertRates(rr.Data, r)
	if err != nil {
		log.Println(err)
		return nil
	}
	return rates
}


