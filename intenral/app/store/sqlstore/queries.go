package sqlstore

const (
	schemaSQL = `
		CREATE TABLE IF NOT EXISTS rates (
			parse_date TIMESTAMP NOT NULL,
			currency_from VARCHAR(10) NOT NULL,
			currency_to VARCHAR(10) NOT NULL,
			rate VARCHAR(10) NOT NULL,
			PRIMARY KEY(parse_date, currency_from, currency_to)
		);

		CREATE TABLE IF NOT EXISTS token (
			id INTEGER PRIMARY KEY,
			token VARCHAR(32) UNIQUE NOT NULL
		);`
)
