package sqlstore

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/lib/pq" // ...
	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/store"
)

type Store struct {
	db     *sql.DB
	ratesRepository *RatesRepository
	tokenRepository *TokenRepository
}


func CreateDB(databaseURL string) error { 
	if _, err := os.Stat(databaseURL); os.IsNotExist(err) {
		log.Println("Creating rates.db...")
		file, err := os.Create(databaseURL) // Create SQLite file
		if err != nil {
			return err
		}
		file.Close()
		log.Println("rates.db created")
	  }
	return nil
}

func CreateSchema(db *sql.DB) error {
	if _, err := db.Exec(schemaSQL); err != nil {
		return err
	}
	return nil
}

func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

// Rates ...
func (s *Store) Rates() store.RatesRepository { 
	if s.ratesRepository != nil { 
		return s.ratesRepository
	}
	s.ratesRepository = &RatesRepository {
		store: s,
	}
	return s.ratesRepository
}

// Token ...
func (s *Store) Token() store.TokenRepository { 
	if s.tokenRepository != nil { 
		return s.tokenRepository
	}
	s.tokenRepository = &TokenRepository {
		store: s,
	}
	return s.tokenRepository
}