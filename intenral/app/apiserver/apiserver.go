package apiserver

import (
	"database/sql"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/store/sqlstore"
)

var remoteServerURL string

// Start ...
func Start(config *Config) error {
	db, err := newDB(config.DatabaseURL)
	if err != nil { 
		return err
	}

	defer db.Close()
	store := sqlstore.New(db)
	srv := newServer(store)
	remoteServerURL = config.RemoteServerURL
	return http.ListenAndServe(config.BindAddr, srv)
}

func newDB(databaseURL string) (*sql.DB, error) {
	err := sqlstore.CreateDB(databaseURL)
	if err != nil { 
		return nil, err
	}

	db, err := sql.Open("sqlite3", databaseURL)
	if err != nil { 
		return nil, err
	}

	err = sqlstore.CreateSchema(db);
	if err != nil {
		return nil, err
	}

	return db, nil
}